import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './components/index/index.component';
import { LoginComponent } from './components/login/login.component';
import { FarmaciaComponent } from './components/farmacia/farmacia.component';
import { PamiComponent } from './components/pami/pami.component';

const routes: Routes = [
  {path: 'index', component: IndexComponent},
  {path: 'login', component: LoginComponent},
  {path: 'farmacia', component: FarmaciaComponent},
  {path: 'pami', component: PamiComponent},
  {path: '**' , component: IndexComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
