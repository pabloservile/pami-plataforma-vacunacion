import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { BackendService } from '../../services/backend.service';
import { Farmacia } from '../../farmacia';



@Component({
  selector: 'app-farmacia',
  templateUrl: './farmacia.component.html',
  styleUrls: ['./farmacia.component.css']
})
export class FarmaciaComponent implements OnInit {
  numeroVac = 0;
  options: FormGroup;
  farmacia: Farmacia;
  hideRequiredControl = new FormControl(false);
  floatLabelControl = new FormControl('auto');
  agregarVacunas: number;

  constructor(fb: FormBuilder, private service: BackendService) {
    this.options = fb.group({
      hideRequired: this.hideRequiredControl,
      floatLabel: this.floatLabelControl,
    });

    this.farmacia = this.service.getFarmacias().shift();
    this.numeroVac = this.farmacia.cantidadVacuna;
  }

  ngOnInit(): void {

  }

  descontar(): void {
    this.numeroVac = this.numeroVac - 1;
  }

  agregarVac(){}
}
