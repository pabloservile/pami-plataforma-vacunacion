import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { BackendService } from '../../services/backend.service';
import { Farmacia } from '../../farmacia';

@Component({
  selector: 'app-pami',
  templateUrl: './pami.component.html',
  styleUrls: ['./pami.component.css']
})
export class PamiComponent implements OnInit {
  panelOpenState = false;

  options: FormGroup;
  hideRequiredControl = new FormControl(false);
  floatLabelControl = new FormControl('auto');
  displayedColumns: string[] = ['razonSocial', 'direccion', 'nombreVacuna', 'cantidadVacuna'];
  dataSource;
  razonSocial = '';
  cuit = '';
  direccion = '';
  farmacia: Farmacia;


  constructor(fb: FormBuilder, private service: BackendService) {
    this.dataSource = service.getFarmacias();
    this.options = fb.group({
      hideRequired: this.hideRequiredControl,
      floatLabel: this.floatLabelControl,
    });
  }

  ngOnInit(): void {
  }

  crearFarmacia(): void{
    this.farmacia = new Farmacia();
    this.farmacia.razonSocial = this.razonSocial;
    this.farmacia.cuit = this.cuit;
    this.farmacia.direccion = this.direccion;
    console.log(this.farmacia.razonSocial);
    this.service.postFarmacia(this.farmacia);
  }


}
