import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PamiComponent } from './pami.component';

describe('PamiComponent', () => {
  let component: PamiComponent;
  let fixture: ComponentFixture<PamiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PamiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PamiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
