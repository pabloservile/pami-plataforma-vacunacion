import { Component, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { Farmacia } from '../../farmacia';
import {MatSort} from '@angular/material/sort';
import { BackendService } from '../../services/backend.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})

export class IndexComponent implements AfterViewInit {
  displayedColumns: string[] = ['razonSocial', 'direccion', 'nombreVacuna', 'cantidadVacuna'];
  dataSource;
  farmaciasData;


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public backendService: BackendService){
    this.farmaciasData = backendService.getFarmacias();
    console.log(this.farmaciasData);
    this.dataSource = new MatTableDataSource<Farmacia>(this.farmaciasData);
  }

  // tslint:disable-next-line:typedef
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // tslint:disable-next-line:typedef
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}

