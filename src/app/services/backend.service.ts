import { Injectable } from '@angular/core';
import { Farmacia } from '../farmacia';




@Injectable({
  providedIn: 'root'
})
export class BackendService {

  public farmacias: Array<Farmacia> = [
    {razonSocial: 'La nueva crucci', cuit: 'Hydrogen',  nombreVacuna: 'COVID-19', cantidadVacuna: 1664,  direccion: 'asdasdasd'},
    {razonSocial: 'Farmacity',       cuit: 'Helium',    nombreVacuna: 'COVID-19', cantidadVacuna: 2443,  direccion: 'qweqwe'},
    {razonSocial: 'Cityfarma',       cuit: 'Lithium',   nombreVacuna: 'COVID-19', cantidadVacuna: 3756,  direccion: 'dfgdfg'},
    {razonSocial: 'Dr farma',        cuit: 'Beryllium', nombreVacuna: 'COVID-19', cantidadVacuna: 4456 , direccion: 'zxczxc'},
    {razonSocial: 'El farmaceutico', cuit: 'Boron',     nombreVacuna: 'COVID-19', cantidadVacuna: 5346 , direccion: '234234234'},
    {razonSocial: 'El loco farma',   cuit: 'Carbon',    nombreVacuna: 'COVID-19', cantidadVacuna: 6345 , direccion: 'ertert'},
    {razonSocial: 'La gran farma',   cuit: 'Nitrogen',  nombreVacuna: 'COVID-19', cantidadVacuna: 7234 , direccion: 'vbnvbn'},
    {razonSocial: 'Farmax',          cuit: 'Oxygen',    nombreVacuna: 'COVID-19', cantidadVacuna: 4123 , direccion: 'ghjghj'},
    {razonSocial: 'Renfarma',        cuit: 'Fluorine',  nombreVacuna: 'COVID-19', cantidadVacuna: 3222 , direccion: 'bnmbnm'},
    {razonSocial: 'Nicofarma',       cuit: 'Neon',      nombreVacuna: 'COVID-19', cantidadVacuna: 3334 , direccion: 'hjkhjk'}
  ];


  f = new Farmacia();

  constructor() { }

  getFarmacias(): Array<any> {
    return this.farmacias;
  }

  postFarmacia(farmacia: Farmacia): void {
    this.farmacias.push(farmacia);
    console.log(JSON.stringify(this.farmacias));
  }

}
